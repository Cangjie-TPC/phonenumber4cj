## 1.0.1
- 适配 cangjie 0.58.3

## 1.0.0
- 实现电话号码的解析功能
- 实现电话号码格式化功能
- 实现数字查找及号码验证功能
- 实现即时格式化功能
- 实现为指定国家/地区提供有效的示例号码功能
- 实现根据电话号码查询地理位置、时区、运营商等相关信息的功能